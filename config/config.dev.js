module.exports = {
  session: {
    key: 'koa:sess',
    maxAge: 86400000,
    overwrite: true,
    httpOnly: true,
    signed: true,
  },
  redis: {
    host: '127.0.0.1',
    port: '6379'
  },
  mongoose: {
    user: '',
    pass: '',
    host: '127.0.0.1',
    port: 27017,
    database: 'test',
    db: {
        native_parser: true
    },
    server: {
        poolSize: 5
    }
  },
  wsServer: {},
  chat: { namespace: '/' },
  appKeys: ['chat'],
  port: process.env.PORT || 6969,
  wshost: '0.0.0.0'
}
