const Koa = require('koa');
const KoaSession = require('koa-session');
const redisStore = require('koa-redis');
const bodyParser = require('koa-bodyparser');
const compose = require('koa-compose');
const http = require('http');
const mongoose = require('koa-mongoose');

const authRouter = require('./backend/routers/auth');
const ticketRouter = require('./backend/routers/ticket');
const trackingRouter = require('./backend/routers/tracking');
const config = require('./config');

const app = new Koa();

const routes = compose([
  authRouter.routes(),
  ticketRouter.routes(),
  trackingRouter.routes(),
]);

app.keys = config.appKeys;

const sessStore = redisStore(config.redis);
const sessionConfig = {
  store: sessStore,
  ...config.store
};

const session = KoaSession(sessionConfig, app);
app.use(mongoose({
    mongoose: require('mongoose-q')(),
    ...config.mongoose
}))
app.use(session);
app.use(bodyParser());
app.use(routes);

const server = http.Server(app.callback());

server.listen(config.port);
