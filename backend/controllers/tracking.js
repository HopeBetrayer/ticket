const Tracking = require('../models/tracking');
const ResponseBuilder = require('../util/ResponseBuilder');
const mongoose = require('mongoose');

class TrackingController {

  static async create(ctx) {
    const data = ctx.request.body;
    const track = new Tracking(data);
    try {
      await track.save();
      ResponseBuilder.success(ctx, { id: track._id });
    } catch(e) {
      ResponseBuilder.error(ctx, 400, e.message);
    }
  }

  static async update(ctx) {
    const id = ctx.params.id;
    const data = ctx.request.body;
    let ticket;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      ticket = await Ticket.findOne({ _id: id }).exec();
      if (ticket) {
        Object.assign(ticket, data);
        await ticket.save();
        ResponseBuilder.success(ctx, ticket);
      } else {
        ResponseBuilder.error(ctx, 404, 'No ticket found');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }

  static async delete(ctx) {
    const id = ctx.params.id;
    let ticket;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      ticket = await Ticket.findOne({ _id: id }).exec();
      if (ticket) {
        ticket.remove();
        ResponseBuilder.success(ctx, { id });
      } else {
        ResponseBuilder.error(ctx, 404, 'No ticket found');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }

  static async getByUser(ctx) {
    const id = ctx.params.userId;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      const trackingInfo = await Tracking.find({ user: id }).exec();
      ResponseBuilder.success(ctx, trackingInfo);
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }
}
module.exports = TrackingController;
