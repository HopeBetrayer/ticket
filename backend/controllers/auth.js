const User = require('../models/user');
const ResponseBuilder = require('../util/ResponseBuilder');
const UserValidator = require('../validators/user');

class AuthController {
  static async register(ctx) {
    const data = ctx.request.body;
    const user = new User(data);
    const validator = new UserValidator(user);
    user.status = 1;
    try {
      await validator.validate();
      await user.save();
      ResponseBuilder.success(ctx, { id: user._id });
    } catch(e) {
      ResponseBuilder.error(ctx, 400, e.message);
    }
  }

  static async login(ctx) {
    const { username, password } = ctx.request.body;
    const { session } = ctx;
    const user = await User.findOne({ username });
    if (user && user.password === password) {
      session.username = username;
      session.user = user;
      ResponseBuilder.success(ctx, { id: user._id });
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid login or password');
    }
  }

  static async checkAuth(ctx) {
    const { session } = ctx;
    if (session.username && session.user) {
      ResponseBuilder.success(ctx, true);
    } else {
      ResponseBuilder.error(ctx, 401, 'unauthorized');
    }
  }

  static async logout(ctx) {
    ctx.session = null;
  }
}
module.exports = AuthController;
