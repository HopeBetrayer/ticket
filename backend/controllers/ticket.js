const Ticket = require('../models/ticket');
const TicketInfo = require('../models/ticket-info');
const ResponseBuilder = require('../util/ResponseBuilder');
const TicketValidator = require('../validators/ticket');
const mongoose = require('mongoose');

class TicketController {
  static async create(ctx) {
    let data = ctx.request.body;
    if (data.message) {
      data = JSON.parse(data.message);
    }
    const ticket = new Ticket(data);
    const validator = new TicketValidator(ticket);
    console.log(data);
    if (!ticket.status) ticket.status = 0;
    try {
      await validator.validate();
      await ticket.save();
      const ticketInfo = new TicketInfo({ ticket: ticket._id, fields: [] });
      await ticketInfo.save();
      ResponseBuilder.success(ctx, { id: ticket._id });
    } catch(e) {
      ResponseBuilder.error(ctx, 400, e.message);
    }
  }

  static async update(ctx) {
    const id = ctx.params.id;
    let data = ctx.request.body;
    if (data.message) {
      data = JSON.parse(data.message);
    }
    let ticket;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      ticket = await Ticket.findOne({ _id: id }).exec();
      if (ticket) {
        Object.assign(ticket, data);
        await ticket.save();
        ResponseBuilder.success(ctx, ticket);
      } else {
        ResponseBuilder.error(ctx, 404, 'No ticket found');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }
  static async updateInfo(ctx) {
    const id = ctx.params.id;
    const data = ctx.request.body;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      const ticketInfo = await TicketInfo.findOne({ ticket: id }).exec();
      if (ticketInfo) {
        Object.assign(ticketInfo, data);
        await ticketInfo.save();
        ResponseBuilder.success(ctx, ticketInfo);
      } else {
        ResponseBuilder.error(ctx, 404, 'No ticket info found');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }
  static async delete(ctx) {
    const id = ctx.params.id;
    let ticket;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      ticket = await Ticket.findOne({ _id: id }).exec();
      if (ticket) {
        ticket.remove();
        ResponseBuilder.success(ctx, { id });
      } else {
        ResponseBuilder.error(ctx, 404, 'No ticket found');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }
  static async getByUser(ctx) {
    const id = ctx.params.id;
    const idValid = mongoose.Types.ObjectId.isValid(id);
    if (idValid) {
      const tickets = await Ticket.find({ assignee: id, status: 1 }).exec();
      ResponseBuilder.success(ctx, tickets);
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id format');
    }
  }

  static async get(ctx) {
    const id = ctx.params.id;
    let tickets;
    if (id) {
      const idValid = mongoose.Types.ObjectId.isValid(id);
      if (idValid) {
        tickets = await Ticket.find({ _id: id }).exec();
        ResponseBuilder.success(ctx, tickets);
      } else {
        ResponseBuilder.error(ctx, 400, 'Invalid id format');
      }
    } else {
      tickets = await Ticket.find({}).exec();
      ResponseBuilder.success(ctx, tickets);
    }
  }
  static async getFree(ctx) {
    const tickets = await Ticket.find({ status: 0 }).exec();
    ResponseBuilder.success(ctx, tickets);
  }
  static async getInfo(ctx) {
    const id = ctx.params.id;
    if (id) {
      const idValid = mongoose.Types.ObjectId.isValid(id);
      if (idValid) {
        const ticketInfo = await TicketInfo.findOne({ ticket: id }).exec();
        ResponseBuilder.success(ctx, ticketInfo);
      } else {
        ResponseBuilder.error(ctx, 400, 'Invalid id format');
      }
    } else {
      ResponseBuilder.error(ctx, 400, 'Invalid id');
    }
  }
}
module.exports = TicketController;
