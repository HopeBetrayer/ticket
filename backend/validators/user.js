const User = require('../models/user');

class UserValidator {
  constructor(user) {
    this.user = user;
  }
  async validateUnique() {
    const { username } = this.user;
    const existingUser = await User.findOne({ username });
    if (existingUser) {
      throw new Error('This username is already taken');
    }
  }
  validateRequired() {
    const { username, password } = this.user;
    if (!username) {
      throw new Error('No username provided');
    }
    if (!password) {
      throw new Error('No password provided');
    }
  }
  validateUsername() {
    const { username } = this.user;
    const usernameValid = /\+\d\([\d]{3}\)\s[\d]{3}\-[\d]{2}\-[\d]{2}/ig.test(username);
    if (!usernameValid) {
      throw new Error('Username is invalid');
    }
  }
  validatePassword() {
    const { password } = this.user;
    if (password.length < 4) {
      throw new Error('Password must be at least 4 characters');
    }
    if (password.length > 15) {
      throw new Error('Password must be not longer than 15 characters');
    }
    if (!/^[A-Za-z0-9]+$/ig.test(password)) {
      throw new Error('Password can only contain latin letters and digits');
    }
  }
  async validate() {
    this.validateRequired();
    this.validateUsername();
    await this.validateUnique();
    this.validatePassword();
  }
}

module.exports = UserValidator;
