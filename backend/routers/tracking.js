const router = require('koa-router');
const TrackingController = require('../controllers/tracking');
const trackingRouter = router({ prefix: '/tracking' });

trackingRouter
  .post('/', TrackingController.create)
  .get('/:userId', TrackingController.getByUser)

module.exports = trackingRouter;
