const router = require('koa-router');
const AuthController = require('../controllers/auth');
const authRouter = router({ prefix: '/auth' });

authRouter
  .post('/register', AuthController.register)
  .post('/', AuthController.login)
  .get('/', AuthController.checkAuth)
  .delete('/', AuthController.logout);

module.exports = authRouter;
