const router = require('koa-router');
const TicketController = require('../controllers/ticket');
const ticketRouter = router({ prefix: '/ticket' });

ticketRouter
  .post('/', TicketController.create)
  .get('/', TicketController.get)
  .get('/free', TicketController.getFree)
  .get('/:id', TicketController.get)
  .get('/user/:id', TicketController.getByUser)
  .get('/info/:id', TicketController.getInfo)
  .put('/:id', TicketController.update)
  .put('/info/:id', TicketController.updateInfo)
  .delete('/:id', TicketController.delete);

module.exports = ticketRouter;
