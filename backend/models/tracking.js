const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: 'User id is required',
  },
  coords: {
    lat: Number,
    lng: Number,
  },
  time: {
    type: Number,
    default: Date.now,
  }
});

module.exports = mongoose.model('Tracking', schema);
