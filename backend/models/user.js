const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const schema = new mongoose.Schema({
  username: {
    type: String,
    required: "Username required",
    unique: true,
  },
  password: {
    type: String,
    required: "Password required",
    min: [6, 'Password is too short'],
    max: [20, 'Password is too long'],
  },
  status: {
    type: Boolean
  }
});

module.exports = mongoose.model('User', schema);
