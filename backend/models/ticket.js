const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  title: {
    type: String,
    default: 'Новая заявка'
  },
  description: {
    type: String,
  },
  status: {
    type: Number,
    default: 0,
  },
  creationDate: {
    type: Number,
    default: Date.now,
  },
  comment: {
    type: String,
  },
  address: {
    type: String,
  },
  coords: {
    lat: {
      type: Number,
      default: 0,
    },
    lng: {
      type: Number,
      default: 0
    }
  },
  contactInfo: {
    contactPerson: {
      firstName: String,
      lastName: String,
      middleName: String,
    },
    contactPhone: String,
    additionalInfo: String,
  },
  requestor: String,
  assignee: String,
});

module.exports = mongoose.model('Ticket', schema);
