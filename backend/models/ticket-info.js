const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  ticket: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  fields: [{
    title: String,
    dataType: {
      type: String,
      enum: ['Number', 'String', 'File', 'Boolean'],
      default: 'String'
    },
    value: {
      type: mongoose.Schema.Types.Mixed,
    }
  }]
});

module.exports = mongoose.model('TicketInfo', schema);
